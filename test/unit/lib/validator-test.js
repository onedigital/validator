import fs from 'fs';
import path from 'path';

describe('Validator', () => {
  describe('MKT without erros', () => {
    it('html sem erros', (done) => {
      const htmlFile = path.join(__dirname, '../../../emails/teste-1/index.html');

      fs.readFile(htmlFile, 'utf8', (error, data) => {
        if (error) {
          throw 'Unable to read file';
        }

        validator(data, (res) => {
          // console.log(res.validation);
          expect(res.w3c).to.be.eql('Não foram encontrado erros no W3C');
          expect(res.validation[0]).to.be.eql('Não existe erro no script do html');

          done();
        });
      });

    });

  });

  describe('MKT with ALT erros', () => {
    it('deve retornar erro de alt', (done) => {
      const htmlFile = path.join(__dirname, '../../../emails/teste-2/index.html');

      fs.readFile(htmlFile, 'utf8', (error, data) => {
        if (error) {
          throw 'Unable to read file';
        }

        validator(data, (res) => {
          // console.log(res.validation[0]);
          expect(res.w3c).to.be.eql('Não foram encontrado erros no W3C');
          expect(res.validation[0]).to.be.eql('Na tag <img> da linha 29 não tem o css-inline style="display: block;"');

          done();
        });
      });

    });

  });
});
