import fs from 'fs';
import Path from 'path';
import Validator from './lib/validator';

const htmlFile = Path.join(__dirname, './index.html');

fs.readFile(htmlFile, 'utf8', (error, data) => {
  Validator(data)
    .then((res) => {
      console.log(res);
    });
});
