'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); // import request from 'superagent';


var _got = require('got');

var _got2 = _interopRequireDefault(_got);

var _formData = require('form-data');

var _formData2 = _interopRequireDefault(_formData);

var _w3cTranslate = require('./w3c-translate');

var _w3cTranslate2 = _interopRequireDefault(_w3cTranslate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://validator.w3.org/nu/
// https://validator.nu/
/**
 * Valida o html usando as regras do W3C
 *
 * @class W3C
 * @constructor
 */
var W3C = function () {
  function W3C(html) {
    _classCallCheck(this, W3C);

    this.url = 'https://validator.w3.org/check';
    this.context = html;
    this.fragment = html;
    this.options = {
      out: 'json',
      parser: 'html4tr',
      laxtype: 'yes',
      preset: 'http://s.validator.nu/xhtml10/xhtml-transitional.rnc http://s.validator.nu/html4/assertions.sch http://c.validator.nu/all-html4/',
      schema: '(?:(?:(?:https?://\S+)|(?:data:\S+))(?:\s+(?:(?:https?://\S+)|(?:data:\S+)))*)?',
      content: html
    };
    this.message = {
      found: 'Erros encontrados na validação do W3C',
      notFound: 'Não foram encontrado erros no W3C'
    };
  }

  _createClass(W3C, [{
    key: 'start',
    value: function start(callback) {
      var _this = this;

      var msgArr = [];

      this.getRequest(this.url, function (res) {
        if (res.length > 0) {

          msgArr.push({
            msg: '' + _this.message.found
          });

          res.forEach(function (element) {
            msgArr.push({
              line: '' + element.lastLine,
              msg: '' + element.message
            });
          });
        } else {
          msgArr.push({
            msg: '' + _this.message.notFound
          });
        }

        if (callback != null) {
          callback(msgArr);
        }
      });
    }
  }, {
    key: 'getRequest',
    value: function getRequest(url, callback) {
      var _this2 = this;

      var body = new _formData2.default();
      body.append('out', this.options.out);
      body.append('schema', this.options.schema);
      body.append('preset', this.options.preset);
      body.append('parser', this.options.parser);
      body.append('laxtype', this.options.laxtype);
      body.append('content', this.options.content);

      _got2.default.post('https://validator.w3.org/check', { body: body }).then(function (res) {
        var dataJSON = JSON.parse(res.body);
        console.log(dataJSON.messages);

        var result = dataJSON.messages.filter(function (element) {
          if (element.type === 'error') {
            var scapeErrors = [/No space between attributes./gi, /Bad value “{{(.*?)}}” for attribute “href” on element “a”: Illegal character in path segment: “{” is not allowed./gi];

            if (!element.message.match(scapeErrors[1])) {
              if (!element.message.match(scapeErrors[1])) {
                element.message = _this2.dictionary(element.message);
                element.line = element.lastLine;

                return element;
              }
            }
          }
        });

        if (callback != null) {
          callback(result);
        }
      }).catch(function (error) {
        if (callback != null) {
          callback(error);
        }
      });
    }
  }, {
    key: 'dictionary',
    value: function dictionary(msg) {
      _w3cTranslate2.default.i8n.filter(function (element) {
        if (element.en.match(/“%\d\$S”/gi) != null && msg.match(/“\w*\”/gi) != null) {
          element.en = element.en.replace(/“%\d\$S”/gi, msg.match(/“\w*\”/gi));
          element.pt = element.pt.replace(/“%\d\$S”/gi, msg.match(/“\w*\”/gi));
        }

        if (element.en === msg) {
          msg = element.pt;
        }
      });

      return msg;
    }
  }]);

  return W3C;
}();

exports.default = function (html) {
  return new W3C(html);
};