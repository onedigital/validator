'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
// import rl from 'readline';


var _stringToStream = require('string-to-stream');

var _stringToStream2 = _interopRequireDefault(_stringToStream);

var _messages = require('./messages');

var _messages2 = _interopRequireDefault(_messages);

var _readliine = require('./readliine');

var _readliine2 = _interopRequireDefault(_readliine);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Valida o html usando regras defenidas
 *
 * @class Validation
 * @constructor
 */
var Validation = function () {
  /**
   * Construtor da classe
   *
   * @method constructor
   *
   * @param {String} htmlStr String do HTML
   */
  function Validation(htmlStr) {
    _classCallCheck(this, Validation);

    this.html = htmlStr.toLowerCase();
  }

  /**
   * Metodo que inicia a aplicação
   *
   * @method start
   *
   * @param {Function} callback Uma função de callback para os erros do html
   */


  _createClass(Validation, [{
    key: 'start',
    value: function start(callback) {
      this.checkErros(this.html, function (result) {
        if (callback != null) {
          callback(result);
        }
      });
    }

    /**
     * Verifica se tem erros no html e retorna uma objeto no callback
     *
     * @method checkErros
     *
     * @param {String} str HTML a ser verificado no formato string
     * @param {Function} callback Uma função de callback para os erros do html
     */

  }, {
    key: 'checkErros',
    value: function checkErros(str, callback) {
      var errorsFounded = 0;
      var msgArr = [];

      // console.log('oiiiiiiiiiii')

      this.configureReadLineResult(str, function (result) {
        // console.log(str);

        result.forEach(function (node, i) {
          var tag = node.tag;
          var line = node.line;
          var tagName = node.tagName;

          if (tag) {
            if (tag.match(_messages2.default.alt.reg)) {
              errorsFounded++;

              msgArr.push({
                line: line,
                msg: '' + _messages2.default.alt.message.replace('{{line}}', line),
                tag: tag
              });
            }

            if (tag.match(_messages2.default.display.reg)) {
              errorsFounded++;

              msgArr.push({
                line: line,
                msg: '' + _messages2.default.display.message.replace('{{line}}', line),
                tag: tag
              });
            }

            if (tag.match(_messages2.default.border.reg)) {
              errorsFounded++;

              msgArr.push({
                line: line,
                msg: '' + _messages2.default.border.message.replace('{{line}}', line),
                tag: tag
              });
            }

            if (tag.match(_messages2.default.rowspan.reg)) {
              errorsFounded++;

              msgArr.push({
                line: line,
                msg: '' + _messages2.default.rowspan.message.replace('{{line}}', line),
                tag: tag
              });
            }

            if (tag.match(_messages2.default.colspan.reg)) {
              errorsFounded++;

              msgArr.push({
                line: line,
                msg: '' + _messages2.default.colspan.message.replace('{{line}}', line),
                tag: tag
              });
            }

            if (tag.match(_messages2.default.div.reg)) {
              errorsFounded++;

              msgArr.push({
                line: line,
                msg: '' + _messages2.default.div.message.replace('{{line}}', line),
                tag: tag
              });
            }

            if (tag.match(_messages2.default.tagfont.reg)) {
              errorsFounded++;

              msgArr.push({
                line: line,
                msg: '' + _messages2.default.tagfont.message.replace('{{line}}', line),
                tag: tag
              });
            }

            if (tagName == 'meta') {
              if (!tag.match(_messages2.default.utf8.reg)) {
                errorsFounded++;

                msgArr.push({
                  line: line,
                  msg: '' + _messages2.default.utf8.message,
                  tag: tag
                });
              }
            }

            if (tagName == 'html') {
              if (!str.match(_messages2.default.doctype.reg)) {
                errorsFounded++;

                msgArr.push({
                  line: '1',
                  msg: '' + _messages2.default.doctype.message,
                  tag: '<!DOCTYPE>'
                });
              }
            }

            if (tagName == 'td') {
              if (tag.match(_messages2.default.legalTextTd.reg)) {
                var tagLegal = str.substring(node.startOffset, node.endTag.endOffset);
                // console.log(tagLegal);

                if (!tagLegal.match(_messages2.default.legaltext.reg)) {
                  errorsFounded++;

                  msgArr.push({
                    line: result[i].line,
                    msg: '' + _messages2.default.legaltext.message.replace('{{line}}', line),
                    tag: result[i + 1].tag
                  });
                } else {

                  if (tagLegal.match(_messages2.default.hasLinkInLegalText.reg) !== null) {
                    errorsFounded++;

                    msgArr.push({
                      line: result[i].line,
                      msg: '' + _messages2.default.hasLinkInLegalText.message.replace('{{line}}', line),
                      tag: line
                    });
                  } else {
                    if (tagLegal.match(_messages2.default.ilegalLink.reg) !== null) {
                      errorsFounded++;

                      msgArr.push({
                        line: result[i + 2].line,
                        msg: '' + _messages2.default.ilegalLink.message,
                        tag: result[i + 1].tag
                      });
                    }

                    if (tagLegal.match(_messages2.default.hasTagSpan.reg) === null) {
                      if (tagLegal.match(_messages2.default.link.reg) !== null) {
                        errorsFounded++;

                        msgArr.push({
                          line: result[i + 2].line,
                          msg: '' + _messages2.default.link.message,
                          tag: result[i + 1].tag
                        });
                      }
                    }
                  }
                }
              }
            }
          }
        });

        // console.log(errorsFounded, Msg.hasLinkInLegalText.reg, Msg.ilegalLink.reg);
        console.log(errorsFounded);
      });

      callback(msgArr);
    }
  }, {
    key: 'configureReadLineResult',
    value: function configureReadLineResult(str, callback) {
      var readline = (0, _readliine2.default)(str);
      var result = [];

      readline.forEach(function (node) {
        if (node.nodeName === '#text') {
          var textNode = node.value.replace(/[-[\]{}()*+?.,\\^$|#\s]/gi, ' ').replace(/\s+/g, ' ');

          if (textNode !== ' ') {
            result.push({
              tagName: node.nodeName,
              text: textNode,
              line: node.line
            });
          }
        } else {
          if (node.tag) {
            result.push({
              tagName: node.nodeName,
              tag: node.tag,
              endTag: node.endTag,
              startOffset: node.startOffset,
              line: node.line
            });
          }
        }
      });

      callback(result);
    }

    /**
     * Pega o html e retorna um objeto com uma lista de tags e os numeros das linhas
     *
     * @method readline
     *
     * @param {String} str HTML no formato string
     * @param {Function} callback Função para o retorno do resultado
     */
    /*readline(str, callback) {
      let newStr = str.replace(/((alt=")((.*(\r\n|(\r\n(\s+|)))+.*)|(.*\s.*))("))/g, function (m) {
        let reg = m.toString().replace(/(\r\n)+|\r+|\n+|\t+/i, ' ');
        reg = reg.replace(/\s\s+/g, ' ');
          return reg;
      });
        const myInterface = rl.createInterface({
        input: stringToStream(newStr)
      });
        let line = 0;
      let objTags = [];
        myInterface.on('line', (text) => {
        line++;
        // console.log('-> ' + text);
        objTags.push({
          tag: text,
          line: line
        });
      });
        myInterface.on('close', () => {
        if (callback != null) {
          callback(objTags);
        }
      });
    }*/

  }, {
    key: 'without',
    value: function without(array, what) {
      return array.filter(function (element) {
        return element !== what;
      });
    }
  }]);

  return Validation;
}();

/*
{
  w3c: '',
  validation: [
    '',...
  ]
}

*/

exports.default = function (htmlStr) {
  return new Validation(htmlStr);
};