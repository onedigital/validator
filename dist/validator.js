'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _promise = require('promise');

var _promise2 = _interopRequireDefault(_promise);

var _w3c = require('./w3c');

var _w3c2 = _interopRequireDefault(_w3c);

var _validation = require('./validation');

var _validation2 = _interopRequireDefault(_validation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (html) {
  return new _promise2.default(function (resolve, reject) {
    (0, _w3c2.default)(html).start(function (w3cResponse) {
      if (typeof w3cResponse == 'string') {
        reject(w3cResponse);
      }

      (0, _validation2.default)(html).start(function (validationResponse) {
        var formatedData = {
          w3c: w3cResponse,
          validation: validationResponse
        };

        resolve(formatedData);
      });
    });
  });
};