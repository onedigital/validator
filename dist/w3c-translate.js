'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  i8n: [{
    en: 'Quirky doctype. Expected “<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">”.',
    pt: 'Peculiar doctype. Esperado “<! DOCTYPE HTML PUBLIC "-// W3C // DTD HTML 4.01 Transitional // EN" "http://www.w3.org/TR/html4/loose.dtd">”.'
  }, {
    en: 'Element “img” is missing required attribute “alt”.',
    pt: 'O elemento "img" está faltando o atributo obrigatório "alt".'
  }, {
    en: 'The character encoding of a framed document was not declared. The document ' + 'may appear different if viewed without the document framing it.',
    pt: 'A codificação de caracteres de um documento em um frame não foi declarada. O ' + 'documento pode ser exibido de forma diferente se aberto fora de um frame.'

  }, {
    en: 'The character encoding of the plain text document was not declared. The ' + 'document will render with garbled text in some ptowser configurations if the ' + 'document contains characters from outside the US-ASCII range. The character ' + 'encoding of the file needs to be declared in the transfer protocol or file ' + 'needs to use a byte order mark as an encoding signature.',
    pt: 'A : codificação de caracteres do documento de texto sem formatação não foi ' + 'declarada. O documento será exibido com texto corrompido em algumas ' + 'configurações de navegadores se o documento contiver caracteres fora da ' + 'faixa US-ASCII. A codificação de caracteres do arquivo deve ser declarada no ' + 'protocolo de transferência ou o arquivo precisa usar uma indicação da ordem ' + 'de bytes (BOM) como uma assinatura da codificação.'

  }, {
    en: 'The character encoding of the HTML document was not declared. The document ' + 'will render with garbled text in some ptowser configurations if the document ' + 'contains characters from outside the US-ASCII range. The character encoding ' + 'of the page must be declared in the document or in the transfer protocol.',
    pt: 'A : codificação de caracteres do documento HTML não foi declarada. O documento ' + 'será exibido com texto corrompido em algumas configurações de navegadores se ' + 'o documento contiver caracteres fora da faixa US-ASCII. A codificação de ' + 'caracteres da página deve ser declarada no documento ou protocolo de ' + 'transferência.'

  }, {
    en: 'The character encoding declaration of the framed HTML document was not found ' + 'when prescanning the first 1024 bytes of the file. When viewed without the ' + 'document framing it, the page will reload automatically. The encoding ' + 'declaration needs to be moved to be within the first 1024 bytes of the file.',
    pt: 'A declaração : da codificação de caracteres do documento HTML em um frame não ' + 'foi encontrada ao verificar os primeiros 1024 bytes do arquivo. Ao ser ' + 'aberta fora de um frame, a página será automaticamente recarregada. A ' + 'declaração da codificação precisa ser movida para os primeiros 1024 bytes do ' + 'arquivo.'

  }, {
    en: 'The character encoding declaration of the HTML document was not found when ' + 'prescanning the first 1024 bytes of the file. When viewed in a differently-' + 'configured ptowser, this page will reload automatically. The encoding ' + 'declaration needs to be moved to be within the first 1024 bytes of the file.',
    pt: 'A declaração : da codificação de caracteres do documento HTML não foi ' + 'encontrada ao verificar os primeiros 1024 bytes do arquivo. Ao ser aberta em ' + 'um navegador configurado de forma diferente, a página será automaticamente ' + 'recarregada. A declaração da codificação precisa ser movida para os ' + 'primeiros 1024 bytes do arquivo.'

  }, {
    en: 'The page was reloaded, because the character encoding declaration of the ' + 'HTML document was not found when prescanning the first 1024 bytes of the ' + 'file. The encoding declaration needs to be moved to be within the first 1024 ' + 'bytes of the file.',
    pt: 'A : página foi recarregada porque a declaração da codificação de caracteres do ' + 'documento HTML não foi encontrada ao verificar os primeiros 1024 bytes do ' + 'arquivo. A declaração da codificação precisa ser movida para os primeiros ' + '1024 bytes do arquivo.'

  }, {
    en: 'The character encoding declaration of document was found too late for it to ' + 'take effect. The encoding declaration needs to be moved to be within the ' + 'first 1024 bytes of the file.',
    pt: 'A : declaração da codificação de caracteres do documento não foi encontrada a ' + 'tempo de fazer efeito. Ela precisa ser movida para os primeiros 1024 bytes ' + 'do arquivo.'

  }, {
    en: 'An unsupported character encoding was declared for the HTML document using a ' + 'meta tag. The declaration was ignored.',
    pt: 'Uma codificação de caracteres não suportada foi declarada para o documento ' + 'HTML através de uma tag meta. A declaração foi ignorada.'

  }, {
    en: 'An unsupported character encoding was declared on the transfer protocol ' + 'level. The declaration was ignored.',
    pt: 'Uma : codificação de caracteres não suportada foi declarada no protocolo de ' + 'transferência. A declaração foi ignorada.'

  }, {
    en: 'Detected UTF-16-encoded Basic Latin-only text without a byte order mark and ' + 'without a transfer protocol-level declaration. Encoding this content in ' + 'UTF-16 is inefficient and the character encoding should have been declared ' + 'in any case.',
    pt: 'Detectado : texto Latim Básico codificado em UTF-16 sem uma indicação da ordem ' + 'de bytes (BOM) e sem uma declaração no protocolo de transferência. Codificar ' + 'este conteúdo em UTF-16 é ineficiente e a codificação de caracteres deveria ' + 'ser declarada de qualquer forma.'

  }, {
    en: 'A meta tag was used to declare the character encoding as UTF-16. This was ' + 'interpreted as an UTF-8 declaration instead.',
    pt: 'Uma tag : meta foi usada para declarar a codificação de caracteres como ' + 'UTF-16. No lugar, ela foi interpretada como uma declaração UTF-8.'

  }, {
    en: 'A meta tag was used to declare the character encoding as x-user-defined. ' + 'This was interpreted as a windows-1252 declaration instead for compatibility ' + 'with intentionally mis-encoded legacy fonts. This site should migrate to ' + 'Unicode.',
    pt: 'Uma : tag meta foi usada para declarar a codificação de caracteres como x-user-' + 'defined. No lugar, ela foi interpretada como uma declaração windows-1252 ' + 'para compatibilidade com fontes antigas intencionalmente codificadas ' + 'incorretamente. Este site deve migrar para Unicode.'

  }, {
    en: 'Garbage after “</”.',
    pt: 'Caracteres sem significado após “</”.'

  }, {
    en: 'Saw “</>”. Probable causes: Unescaped “<” (escape as “&lt;”) or mistyped end ' + 'tag.',
    pt: 'Encontrado “</>”. Causas prováveis: “<” literal (codifique como “&lt;”) ou ' + 'tag de fechamento acidental.'

  }, {
    en: 'Character reference was not terminated by a semicolon.',
    pt: 'Referência a caractere não terminada em ponto e vírgula.'

  }, {
    en: 'No digits in numeric character reference.',
    pt: 'Nenhum dígito em uma referência a caractere numérico.'

  }, {
    en: '“>” in system identifier.',
    pt: '“>” em identificador de sistema.'

  }, {
    en: '“>” in public identifier.',
    pt: '“>” em identificador público.'

  }, {
    en: 'Nameless doctype.',
    pt: 'Doctype sem nome.'

  }, {
    en: 'Consecutive hyphens did not terminate a comment. “--” is not permitted ' + 'inside a comment, but e.g. “- -” is.',
    pt: 'Hifens : consecutivos não terminaram um comentário. “--” não é permitido ' + 'dentro de um comentário. Mas “- -”, por exemplo, é.'

  }, {
    en: 'Premature end of comment. Use “-->” to end a comment properly.',
    pt: 'Fim de comentário prematuro. Use “-->” para fechar um comentário ' + 'corretamente.'

  }, {
    en: 'Bogus comment.',
    pt: 'Comentário inválido.'

  }, {
    en: '“<” in an unquoted attribute value. Probable cause: Missing “>” immediately ' + 'before.',
    pt: '“ :<” em um valor sem aspas de um atributo. Causa provável: falta de “>” ' + 'imediatamente antes.'

  }, {
    en: '“`” in an unquoted attribute value. Probable cause: Using the wrong ' + 'character as a quote.',
    pt: '“`” : em um valor sem aspas de um atributo. Causa provável: uso do caractere ' + 'errado como aspas.'

  }, {
    en: 'Quote in an unquoted attribute value. Probable causes: Attributes running ' + 'together or a URL query string in an unquoted attribute value.',
    pt: 'Aspas : em um valor sem aspas de um atributo. Causas prováveis: mistura de ' + 'atributos ou URL em um valor sem aspas de um atributo.'

  }, {
    en: '“=” in an unquoted attribute value. Probable causes: Attributes running ' + 'together or a URL query string in an unquoted attribute value.',
    pt: '“=” : em um valor sem aspas de um atributo. Causas prováveis: mistura de ' + 'atributos ou URL em um valor sem aspas de um atributo.'

  }, {
    en: 'A slash was not immediately followed by “>”.',
    pt: 'Uma barra não foi seguida imediatamente por “>”.'

  }, {
    en: 'No space between attributes.',
    pt: 'Não há espaço entre atributos.'

  }, {
    en: '“<” at the start of an unquoted attribute value. Probable cause: Missing “>” ' + 'immediately before.',
    pt: '“<” no : início de um valor sem aspas de um atributo. Causa provável: falta de ' + '“>” imediatamente antes.'

  }, {
    en: '“`” at the start of an unquoted attribute value. Probable cause: Using the ' + 'wrong character as a quote.',
    pt: '“ :`” no início de um valor sem aspas de um atributo. Causa provável: uso do ' + 'caractere errado como aspas.'

  }, {
    en: '“=” at the start of an unquoted attribute value. Probable cause: Stray ' + 'duplicate equals sign.',
    pt: '“=” : no início de um valor sem aspas de um atributo. Causa provável: sinal de ' + 'igualdade sem utilidade duplicado.'

  }, {
    en: 'Attribute value missing.',
    pt: 'Falta do valor do atributo.'

  }, {
    en: 'Saw “<” when expecting an attribute name. Probable cause: Missing “>” ' + 'immediately before.',
    pt: 'Encontrado : “<” quando era esperado um nome de atributo. Causa provável: ' + 'falta de “>” imediatamente antes.'

  }, {
    en: 'Saw “=” when expecting an attribute name. Probable cause: Attribute name ' + 'missing.',
    pt: 'Encontrado : “=” quando era esperado um nome de atributo. Causa provável: ' + 'falta de um nome de atributo.'

  }, {
    en: 'Bad character after “<”. Probable cause: Unescaped “<”. Try escaping it as ' + '“&lt;”.',
    pt: 'Caractere errado depois de “<”. Causa provável: “<” literal. Tente codificá-' + 'lo como “&lt;”.'

  }, {
    en: 'Saw “<>”. Probable causes: Unescaped “<” (escape as “&lt;”) or mistyped ' + 'start tag.',
    pt: 'Encontrado : “<>”. Causas prováveis: “<” literal (codifique como “&lt;”) ou ' + 'tag de início acidental.'

  }, {
    en: 'Saw “<?”. Probable cause: Attempt to use an XML processing instruction in ' + 'HTML. (XML processing instructions are not supported in HTML.)',
    pt: 'Encontrado “<?”. Causa provável: tentativa de usar uma instrução de ' + 'processamento XML em HTML. (instruções de processamento XML não são válidas ' + 'em HTML.)'

  }, {
    en: 'The string following “&” was interpreted as a character reference. (“&” ' + 'probably should have been escaped as “&amp;”.)',
    pt: 'Os : caracteres após “&” foram interpretados como referência a um caractere. ' + '(“&” possivelmente deveria ter sido codificado como “&amp;”.)'

  }, {
    en: 'Named character reference was not terminated by a semicolon. (Or “&” should ' + 'have been escaped as “&amp;”.)',
    pt: 'Referência a nome de caractere não terminada com ponto e vírgula. (Ou “&” ' + 'deveria ter sido codificado como “&amp;”.)'

  }, {
    en: '“&” did not start a character reference. (“&” probably should have been ' + 'escaped as “&amp;”.)',
    pt: '“ :&” não iniciou uma referência a caractere. (possivelmente “&” deveria ter ' + 'sido codificado como “&amp;”.)'

  }, {
    en: 'Saw a quote when expecting an attribute name. Probable cause: “=” missing ' + 'immediately before.',
    pt: 'Encontrada : aspas quando era esperado um nome de atributo. Causa provável: ' + 'falta de “=” imediatamente antes.'

  }, {
    en: '“<” in attribute name. Probable cause: “>” missing immediately before.',
    pt: '“<” no nome de um atributo. Causa provável: falta de “>” imediatamente antes.'

  }, {
    en: 'Quote in attribute name. Probable cause: Matching quote missing somewhere ' + 'earlier.',
    pt: 'Aspas : no nome de um atributo. Causa provável: falta da aspas correspondente ' + 'em algum lugar antes.'

  }, {
    en: 'Expected a public identifier but the doctype ended.',
    pt: 'Esperado identificador público mas o doctype foi fechado.'

  }, {
    en: 'Bogus doctype.',
    pt: 'Doctype inválido.'

  }, {
    en: 'End tag had attributes.',
    pt: 'Tag de fechamento possui atributos.'

  }, {
    en: 'Stray “/” at the end of an end tag.',
    pt: '“/” sem utilidade no final de uma tag de fechamento.'

  }, {
    en: 'Character reference expands to a non-character.',
    pt: 'Referência a caractere expandiu-se para um caractere inválido.'

  }, {
    en: 'Character reference expands to a surrogate.',
    pt: 'Referência do carácter expande para substituto.'

  }, {
    en: 'Character reference expands to a control character.',
    pt: 'Referência a caractere expandiu-se para um caractere de controle.'

  }, {
    en: 'A numeric character reference expanded to carriage return.',
    pt: 'Uma referência a caractere numérico expandiu-se para um CR (carriage return).'

  }, {
    en: 'A numeric character reference expanded to the C1 controls range.',
    pt: 'Uma referência a caractere numérico expandiu-se para o intervalo de controle ' + 'C1.'

  }, {
    en: 'End of file inside public identifier.',
    pt: 'Fim de arquivo dentro do identificador público.'

  }, {
    en: 'End of file inside comment.',
    pt: 'Fim de arquivo dentro de um comentário.'

  }, {
    en: 'End of file inside doctype.',
    pt: 'Fim de arquivo dentro de um doctype.'

  }, {
    en: 'End of file reached when inside an attribute value. Ignoring tag.',
    pt: 'Fim de arquivo alcançado dentro do valor de um atributo. Ignorando tag.'

  }, {
    en: 'End of file occurred in an attribute name. Ignoring tag.',
    pt: 'Fim de arquivo ocorreu no nome de um atributo. Ignorando tag.'

  }, {
    en: 'Saw end of file without the previous tag ending with “>”. Ignoring tag.',
    pt: 'Encontrado fim de arquivo sem que a tag anterior fosse fechada com “>”. ' + 'Ignorando tag.'

  }, {
    en: 'End of file seen when looking for tag name. Ignoring tag.',
    pt: 'Fim de arquivo encontrado ao procurar o nome da tag. Ignorando tag.'

  }, {
    en: 'End of file inside end tag. Ignoring tag.',
    pt: 'Fim de arquivo dentro de uma tag de fechamento. Ignorando tag.'

  }, {
    en: 'End of file after “<”.',
    pt: 'Fim de arquivo depois de “<”.'

  }, {
    en: 'Character reference outside the permissible Unicode range.',
    pt: 'Referência a caractere fora do intervalo Unicode permitido.'

  }, {
    en: 'Character reference expands to a permanently unassigned code point.',
    pt: 'Referência a caractere expandiu-se para um code point definitivamente sem ' + 'atribuição.'

  }, {
    en: 'Duplicate attribute.',
    pt: 'Atributo duplicado.'

  }, {
    en: 'End of file inside system identifier.',
    pt: 'Fim de arquivo dentro do identificador de sistema.'

  }, {
    en: 'Expected a system identifier but the doctype ended.',
    pt: 'Esperado identificador de sistema mas o doctype foi fechado.'

  }, {
    en: 'Missing space before doctype name.',
    pt: 'Falta de espaço antes do nome do doctype.'

  }, {
    en: '“--!” found in comment.',
    pt: '“--!” encontrado em um comentário.'

  }, {
    en: 'Character reference expands to zero.',
    pt: 'Referência a caractere expandiu-se para zero.'

  }, {
    en: 'No space between the doctype “SYSTEM” keyword and the quote.',
    pt: 'Não há espaço entre a palavra-chave “SYSTEM” do doctype e as aspas.'

  }, {
    en: 'No space between the doctype public and system identifiers.',
    pt: 'Não há espaço entre os identificadores público e de sistema do doctype.'

  }, {
    en: 'No space between the doctype “PUBLIC” keyword and the quote.',
    pt: 'Não há espaço entre a palavra-chave “PUBLIC” do doctype e as aspas.'

  }, {
    en: 'Stray start tag “%1$S”.',
    pt: 'Tag de início “%1$S” sem utilidade.'

  }, {
    en: 'Stray end tag “%1$S”.',
    pt: 'Tag de fechamento “%1$S” sem utilidade.'

  }, {
    en: 'End tag “%1$S” seen, but there were open elements.',
    pt: 'Tag de fechamento “%1$S” encontrada, mas havia elementos abertos.'

  }, {
    en: 'End tag “%1$S” implied, but there were open elements.',
    pt: 'Tag de fechamento “%1$S” implícita, mas havia elementos abertos.'

  }, {
    en: 'A table cell was implicitly closed, but there were open elements.',
    pt: 'Uma célula de uma tabela foi implicitamente fechada, mas havia elementos ' + 'abertos.'

  }, {
    en: 'Stray doctype.',
    pt: 'Doctype sem utilidade.'

  }, {
    en: 'Almost standards mode doctype. Expected “<!DOCTYPE html>”.',
    pt: 'Doctype do modo de quase conformidade com os padrões. Esperado “<!DOCTYPE ' + 'html>”.'

  }, {
    en: 'Quirky doctype. Expected “<!DOCTYPE html>”.',
    pt: 'Doctype do modo de compatibilidade. Esperado “<!DOCTYPE html>”.'

  }, {
    en: 'Non-space character in page trailer.',
    pt: 'Caractere diferente de espaço no fim da página.'

  }, {
    en: 'Non-space after “frameset”.',
    pt: 'Caractere diferente de espaço após “frameset”.'

  }, {
    en: 'Non-space in “frameset”.',
    pt: 'Caractere diferente de espaço em “frameset”.'

  }, {
    en: 'Non-space character after body.',
    pt: 'Caractere diferente de espaço após body.'

  }, {
    en: 'Non-space in “colgroup” when parsing fragment.',
    pt: 'Caractere diferente de espaço em “colgroup” ao processar fragmento.'

  }, {
    en: 'Non-space character inside “noscript” inside “head”.',
    pt: 'Caractere diferente de espaço dentro de “noscript” dentro de “head”.'

  }, {
    en: '“%1$S” element between “head” and “body”.',
    pt: 'Elemento “%1$S” entre “head” e “body”.'

  }, {
    en: 'Start tag seen without seeing a doctype first. Expected “<!DOCTYPE html>”.',
    pt: 'Tag de início encontrada sem encontrar um doctype primeiro. Esperado “<!' + 'DOCTYPE html>”.'

  }, {
    en: 'No “select” in table scope.',
    pt: 'Não há “select” em escopo de tabela.'

  }, {
    en: '“select” start tag where end tag expected.',
    pt: 'Tag de início de “select” onde a tag de fechamento era esperada.'

  }, {
    en: '“%1$S” start tag with “select” open.',
    pt: 'Tag de início de “%1$S” com “select” aberto.'

  }, {
    en: 'Bad start tag “%1$S” in “head”.',
    pt: 'Tag de início “%1$S” errada em “head”.'

  }, {
    en: 'Saw a start tag “image”.',
    pt: 'Encontrada uma tag de início de “image”.'

  }, {
    en: '“isindex” seen.',
    pt: 'Encontrado “isindex”.'

  }, {
    en: 'An “%1$S” start tag seen but an element of the same type was already open.',
    pt: 'Uma tag de início de “%1$S” foi encontrada mas um elemento do mesmo tipo já ' + 'estava aberto.'

  }, {
    en: 'Heading cannot be a child of another heading.',
    pt: 'Um título não pode estar dentro de outro título.'

  }, {
    en: '“frameset” start tag seen.',
    pt: 'Encontrada tag de início “frameset”.'

  }, {
    en: 'No cell to close.',
    pt: 'Nenhuma célula para fechar.'

  }, {
    en: 'Start tag “%1$S” seen in “table”.',
    pt: 'Tag de início de “%1$S” encontrada em “table”.'

  }, {
    en: 'Saw a “form” start tag, but there was already an active “form” element. ' + 'Nested forms are not allowed. Ignoring the tag.',
    pt: 'Encontrada : uma tag de início de “form”, mas já havia um elemento “form” ' + 'ativo. Formulários aninhados não são permitidos. Ignorando tag.'

  }, {
    en: 'Start tag for “table” seen but the previous “table” is still open.',
    pt: 'Encontrada a tag de início “table” mas a “table” anterior ainda está aberta.'

  }, {
    en: '“%1$S” start tag in table body.',
    pt: 'Tag de início de “%1$S” no corpo da tabela.'

  }, {
    en: 'End tag seen without seeing a doctype first. Expected “<!DOCTYPE html>”.',
    pt: 'Tag de fechamento encontrada sem encontrar um doctype primeiro. Esperado “<!' + 'DOCTYPE html>”.'

  }, {
    en: 'Saw an end tag after “body” had been closed.',
    pt: 'Encontrada uma tag de fechamento depois de “body” ser fechada.'

  }, {
    en: '“%1$S” end tag with “select” open.',
    pt: 'Tag de fechamento “%1$S” com “select” aberta.'

  }, {
    en: 'Garbage in “colgroup” fragment.',
    pt: 'Caracteres sem significado no fragmento “colgroup”.'

  }, {
    en: 'End tag “pt”.',
    pt: 'Tag de fechamento “pt”.'

  }, {
    en: 'No “%1$S” element in scope but a “%1$S” end tag seen.',
    pt: 'Nenhum elemento “%1$S” no escopo mas encontrada uma tag de fechamento “%1$S”.'

  }, {
    en: 'HTML start tag “%1$S” in a foreign namespace context.',
    pt: 'Tag de início HTML “%1$S” em um contexto de namespace externo.'

  }, {
    en: '“table” closed but “caption” was still open.',
    pt: '“table” foi fechada mas “caption” continuava aberta.'

  }, {
    en: 'No table row to close.',
    pt: 'Nenhuma linha de tabela para fechar.'

  }, {
    en: 'Misplaced non-space characters inside a table.',
    pt: 'Caracteres diferentes de espaço em local incorreto dentro de uma tabela.'

  }, {
    en: 'Unclosed children in “ruby”.',
    pt: 'Elemento filho não fechado em “ruby”.'

  }, {
    en: 'Start tag “%1$S” seen without a “ruby” element being open.',
    pt: 'Tag de início de “%1$S” encontrada sem um elemento “ruby” aberto.'

  }, {
    en: 'Self-closing syntax (“/>”) used on a non-void HTML element. Ignoring the ' + 'slash and treating as a start tag.',
    pt: 'Sintaxe : de autofechamento (“/>”) usada para um elemento HTML não vazio. ' + 'Ignorando a barra e considerando como tag de início.'

  }, {
    en: 'Unclosed elements on stack.',
    pt: 'Elementos não fechados na pilha.'

  }, {
    en: 'End tag “%1$S” did not match the name of the current open element (“%2$S”).',
    pt: 'Tag de fechamento “%1$S” não corresponde ao nome do elemento aberto atual ' + '(“%2$S”).'

  }, {
    en: 'End tag “%1$S” violates nesting rules.',
    pt: 'Tag de fechamento “%1$S” viola as regras de aninhamento.'

  }, {
    en: 'End tag for “%1$S” seen, but there were unclosed elements.',
    pt: 'Tag de fechamento de “%1$S” encontrada, mas havia elementos não fechados.'
  }, {
    en: 'Unclosed element “%1$S”.',
    pt: 'Elemento não fechado “%1$S”.'
  }, {
    en: 'End of file seen and there were open elements.',
    pt: 'Fim do arquivo visto e há elementos abertos.'
  }]
};