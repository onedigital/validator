# Validator
[![npm](https://img.shields.io/npm/v/npm.svg)]() [![build](https://img.shields.io/badge/build-passing-brightgreen.svg)]() [![licenca](https://img.shields.io/npm/l/express.svg)]()

## O que faz?
Valida os e-mails usando regras determindadas e também usa o W3C como paramentro para verificar erros no html.

### Primeiros passos
Antes de começar o projeto acesso o link de [primeiros passos](https://bitbucket.org/onedigital/docs/src/db3546173868cb6cd28c6b088fa1bc4bf1a511a6/FIRST_STEPS.md?at=master&fileviewer=file-view-default) siga as instruções para configurar todo o ambiente de desenvolvimento.

### Como usar 
Primeiro instale o validator como dependencia no projeto 
```sh
$ npm install --save bitbucket:onedigital/validator
```
Depois basta fazer um import para o projeto

```js
import Validator from 'valitador';
```

##### Exemplo 
segue um exemplo de como utilizar  
```js
import fs from 'fs';
import Validator from 'validador';

fs.readFile('./index.html', 'utf8', (error, data) => {
  Validator(data)
    .then((res) => {
      console.log(res);
    });
});
// retorno
/*
{
  w3c: [
    { msg: 'mensagem' }
  ],
  validation: [
    {
      line: 0,
      msg: 'mensagem'
    }
  ]
}
*/
```
