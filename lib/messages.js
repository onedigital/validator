export default {
  error: {
    message: 'Os erros a seguir foram encontrados na programação do email: \r\n'
  },
  noErrors: {
    message: 'Não foram encontrados erros no html'
  },
  span:  {
    reg: /<span.*(>)/gi
  },
  div: {
    reg: /<div.*?(>)/gi,
    message: '<div> não pode ser usado na linha {{line}} mude para <p> ou <span>'
  },
  border: {
    reg: /<a.*?>(\n(\s|)|)(\s+)(<img((?!.*?border:(\s|)0)|.*?alt="(\s|)")[^>]*)(>)(\s+)\1<\/a>/gi,
    message: 'Na tag <img> com link na linha {{line}} precisa ter o css-inline border:0;'
  },
  display: {
    reg: /<img(?![^>]*\bdisplay:(\s|)block)[^>]*?(>)/gi,
    message: 'Na tag <img> da linha {{line}} não tem o css-inline display: block;'
  },
  alt: {
    reg: /(<img((?!.*?alt=(['"]).*?\2)|.*?alt="(\s|)")[^>]*)(>)/gi,
    message: 'Na tag <img> da linha {{line}} não tem a tag alt ou esta em branco;'
  },
  utf8: {// https://regex101.com/r/M8ZoMg/4/
    reg: /<meta(?!\s*(?:name|value)\s*=)[^>]*?charset\s*=[\s"']*(utf-8)[\s"'\/]*>/gi,
    message: 'Na linha {{line}} o charset não está codificado para UTF-8 ou não existe;'
  },
  rowspan: {
    reg: /<tr(.*?)(rowspan="(\s|.*)")>/gi,
    message: 'Não podemos utilizar rowspan na linha {{line}};'
  },
  colspan: {
    reg: /<tr(.*?)(colspan="(\s|.*)")>/gi,
    message: 'Não podemos utilizar colspan na linha {{line}};'
  },
  doctype: {
    reg: /<!doctype.*(>)/gi,
    message: 'O html precisa ter o DOCTYPE no topo do HTML;'
  },
  fontFamily: {
    reg: /<span(?![^>]*\bfont-family:(\s|)arial)[^>]*?(>)/gi,
    message: 'A fonte do texto legal na linha {{line}} deve ser Arial;'
  },
  fontSize: {
    reg: /font-size:(\s|)10px/gi,
    message: 'O tamanho da fonte na {{line}} deve ser 10px;'
  },
  textAlign: {
    reg: /text-align:(\s|)justify/gi,
    message: 'Texto legal precisa ser justificado!'
  },
  legalTextTd: {
    reg: /<td(.*)(style="text-align:(\s|)justify(;|)").*(>)/gi,
    message: 'na {{line}} inicia o texto legal.'
  },
  legaltext: {
    reg: /<span(.*)(style="font-family:(\s|)arial;(\s|)font-size:(\s|)10px(;|).*")(>)/gi,
    message: 'No texto legal na linha {{line}} a font family tem que ser arial e o font size 10px;'
  },
  tagfont: {
    reg: /<font(.*)>/gi,
    message: 'Na linha {{line}} não pode ser usado a tag <font>, use <span> ou <p>;'
  },
  ilegalLink: {
    reg: /(([a-zA-Z]+){2,}\.([a-zA-Z\u00C0-\u00FF]+){2,}(\.([a-zA-Z]+){2,}){1,2})|([a-zA-Z\u00C0-\u00FF]+){2,}(\.([a-zA-Z]+){2,}){1,2}/gi,
    message: 'No texto legal não pode ter link favor formatar de maneira correta usando <span>;'
  },
  hasTagSpan: {
    // reg: /(<span style="display:(\s|)inline-block(;|)">)([a-zA-Z\u00C0-\u00FF]+){2,}\.(<span.*?>&nbsp;<\/span>)(\w+)((\.|\/)\5)((\w+<\/span>|\w+\.\5\w+<\/span>))/gi,
    reg: /(<span style="display:(\s|)inline-block(;|)">(<span.*?>&nbsp;<\/span>)<\/span>)/gi,
    message: 'A formatação do link dentro do texto legal esta errada;'
  },
  link: {
    // (<span style="display:(\s|)inline-block(;|)">([a-zA-Z\u00C0-\u00FF]+)(<span.*?>&nbsp;<\/span>)(([a-zA-Z\u00C0-\u00FF]+))<\/span>)
    // ([a-zA-Z\u00C0-\u00FF]+)(<span.*?>&nbsp;<\/span>)(([a-zA-Z\u00C0-\u00FF]+))
    // reg: /(<span style="display:(\s|)inline-block(;|)">)([a-zA-Z\u00C0-\u00FF]+){2,}\.(<span.*?>&nbsp;<\/span>)(\w+)((\.|\/)\5)((\w+<\/span>|\w+\.\5\w+<\/span>))/gi,
    reg: /(<span style="display:(\s|)inline-block(;|)">((?=<span.*?>&nbsp;<\/span>)([a-zA-Z\u00C0-\u00FF]+)(<span.*?>&nbsp;<\/span>)(([a-zA-Z\u00C0-\u00FF]+)))<\/span>)/gi,
    message: 'A formatação do link dentro do texto legal esta errada;'
  },
  hasLinkInLegalText: {
    reg: /([a-zA-Z\u00C0-\u00FF]+){2,}(\.(\w+){2,})/gi,
    message: 'No texto legal não pode ter link verifique como formatar o link;'
  }
};
// pode ter outra td com span que nao é texto legal
// pode nao ter o texto legal
// verificar se o link do texto legal esta formatado de forma errada ex: .com.br <span>com.<span>br
// todo o span tem que ter o font-family arial
//
