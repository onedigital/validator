const parse5 = require('parse5');

let descendants = [];

const recurseAndAdd = (el, str) => {
  const children = el.childNodes;

  for (let i = 0;  i < children.length; i++) {
    descendants.push(formatedObject(children[i], str));

    if (children[i].childNodes) {
      recurseAndAdd(children[i], str);
    }
  }
};

const formatedObject = (el, str) => {
  const data = {};

  data.nodeName = el.nodeName;

  if (el.value) {
    data.value = el.value;
  }

  if (el.attrs) {
    if (el.attrs.length > 0) {
      data.attrs = el.attrs;
    }
  }

  if (el.__location) {
    if (el.__location.startTag) {
      data.tag = str.substring(el.__location.startTag.startOffset, el.__location.startTag.endOffset).replace(/(\n|\t)/g, '');
    } else {
      if (el.nodeName !== '#text') {
        data.tag = str.substring(el.__location.startOffset, el.__location.endOffset).replace(/(\n|\t)/g, '');
      }
    }

    data.line = el.__location.line;
    data.col = el.__location.col;
    data.startOffset = el.__location.startOffset;
    data.endOffset = el.__location.endOffset;

    if (el.__location.endTag) {
      // data.endTag = el.__location.endTag;
      data.endTag = {
        startOffset: el.__location.endTag.startOffset,
        endOffset: el.__location.endTag.endOffset,
        tag: str.substring(el.__location.endTag.startOffset, el.__location.endTag.endOffset).replace(/(\n|\t)/g, ''),
        line: el.__location.endTag.line
      };

      // if (data.nodeName == 'td')
        // data.fullTag = str.substring(el.__location.startTag.startOffset, el.__location.endTag.endOffset).replace(/(\n|\t)/g, '');
    }
  }

  // console.log(el);

  if (el.parentNode) {
    data.parentNode = {
      nodeName: el.parentNode.nodeName,
    };

    if (el.parentNode.attrs) {
      if (el.parentNode.attrs.length > 0) {
        data.parentNode.attrs = el.parentNode.attrs;
      }
    }
  }

  return data;
};


module.exports = (node) => {
  const document = parse5.parse(node, {
    locationInfo: true,
    treeAdapter: parse5.treeAdapters.default
  });

  const html = document.childNodes;
  descendants = [];

  for (let i = 0; i < html.length; i++) {
    if (html[i].tagName) {
      descendants.push(formatedObject(html[i], node));

      recurseAndAdd(html[i], node);
    }
  }

  return descendants;
};
