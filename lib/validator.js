import Promise from 'promise';

import W3C from './w3c';
import Validation from './validation';

export default (html) => {
  return new Promise((resolve, reject) => {
    W3C(html)
      .start((w3cResponse) => {
        if (typeof w3cResponse == 'string') {
          reject(w3cResponse);
        }

        Validation(html)
          .start((validationResponse) => {
            const formatedData = {
              w3c: w3cResponse,
              validation: validationResponse
            };

            resolve(formatedData);
          });
      });
  });
};
