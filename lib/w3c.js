// import request from 'superagent';
import got from 'got';
import FormData from 'form-data';
import translate from './w3c-translate';

// https://validator.w3.org/nu/
// https://validator.nu/
/**
 * Valida o html usando as regras do W3C
 *
 * @class W3C
 * @constructor
 */
class W3C {
  constructor(html) {
    this.url = 'https://validator.w3.org/check';
    this.context = html;
    this.fragment = html;
    this.options = {
      out: 'json',
      parser: 'html4tr',
      laxtype: 'yes',
      preset: 'http://s.validator.nu/xhtml10/xhtml-transitional.rnc http://s.validator.nu/html4/assertions.sch http://c.validator.nu/all-html4/',
      schema: '(?:(?:(?:https?://\S+)|(?:data:\S+))(?:\s+(?:(?:https?://\S+)|(?:data:\S+)))*)?',
      content: html
    };
    this.message = {
      found: 'Erros encontrados na validação do W3C',
      notFound: 'Não foram encontrado erros no W3C',
    };
  }

  start(callback) {
    let msgArr = [];

    this.getRequest(this.url, (res) => {
      if (res.length > 0) {

        msgArr.push({
          msg: `${this.message.found}`
        });

        res.forEach((element) => {
          msgArr.push({
            line: `${element.lastLine}`,
            msg: `${element.message}`
          });
        });
      } else {
        msgArr.push({
          msg: `${this.message.notFound}`
        });
      }

      if (callback != null) {
        callback(msgArr);
      }
    });
  }

  getRequest(url, callback) {
    const body = new FormData();
    body.append('out', this.options.out);
    body.append('schema', this.options.schema);
    body.append('preset', this.options.preset);
    body.append('parser', this.options.parser);
    body.append('laxtype', this.options.laxtype);
    body.append('content', this.options.content);

    got
      .post('https://validator.w3.org/check', { body: body })
      .then((res) => {
        const dataJSON = JSON.parse(res.body);
        console.log(dataJSON.messages);

        const result = dataJSON.messages.filter((element) => {
          if (element.type === 'error') {
            var scapeErrors = [
              /No space between attributes./gi,
              /Bad value “{{(.*?)}}” for attribute “href” on element “a”: Illegal character in path segment: “{” is not allowed./gi,
            ];

            if (!element.message.match(scapeErrors[1])) {
              if (!element.message.match(scapeErrors[1])) {
                element.message = this.dictionary(element.message);
                element.line = element.lastLine;

                return element;
              }
            }
          }
        });

        if (callback != null) {
          callback(result);
        }
      })
      .catch((error) => {
        if (callback != null) {
          callback(error);
        }
      });
  }

  dictionary(msg) {
    translate.i8n.filter((element) => {
      if (element.en.match(/“%\d\$S”/gi) != null && msg.match(/“\w*\”/gi) != null) {
        element.en = element.en.replace(/“%\d\$S”/gi, msg.match(/“\w*\”/gi));
        element.pt = element.pt.replace(/“%\d\$S”/gi, msg.match(/“\w*\”/gi));
      }

      if (element.en === msg) {
        msg = element.pt;
      }
    });

    return msg;
  }
}

export default (html) => {
  return new W3C(html);
};
