import stringToStream from 'string-to-stream';
// import rl from 'readline';
import Msg from './messages';
import ReadLine from './readliine';



/**
 * Valida o html usando regras defenidas
 *
 * @class Validation
 * @constructor
 */
class Validation {
  /**
   * Construtor da classe
   *
   * @method constructor
   *
   * @param {String} htmlStr String do HTML
   */
  constructor(htmlStr) {
    this.html = htmlStr.toLowerCase();
  }

  /**
   * Metodo que inicia a aplicação
   *
   * @method start
   *
   * @param {Function} callback Uma função de callback para os erros do html
   */
  start(callback) {
    this.checkErros(this.html, (result) => {
      if (callback != null) {
        callback(result);
      }
    });
  }

  /**
   * Verifica se tem erros no html e retorna uma objeto no callback
   *
   * @method checkErros
   *
   * @param {String} str HTML a ser verificado no formato string
   * @param {Function} callback Uma função de callback para os erros do html
   */
  checkErros(str, callback) {
    let errorsFounded = 0;
    const msgArr = [];

    // console.log('oiiiiiiiiiii')

    this.configureReadLineResult(str, (result) => {
      // console.log(str);

      result.forEach((node, i) => {
        let tag = node.tag;
        let line = node.line;
        let tagName = node.tagName;

        if (tag) {
          if (tag.match(Msg.alt.reg)) {
            errorsFounded++;

            msgArr.push({
              line: line,
              msg: `${Msg.alt.message.replace('{{line}}', line)}`,
              tag: tag
            });
          }

          if (tag.match(Msg.display.reg)) {
            errorsFounded++;

            msgArr.push({
              line: line,
              msg: `${Msg.display.message.replace('{{line}}', line)}`,
              tag: tag
            });
          }

          if (tag.match(Msg.border.reg)) {
            errorsFounded++;

            msgArr.push({
              line: line,
              msg: `${Msg.border.message.replace('{{line}}', line)}`,
              tag: tag
            });
          }

          if (tag.match(Msg.rowspan.reg)) {
            errorsFounded++;

            msgArr.push({
              line: line,
              msg: `${Msg.rowspan.message.replace('{{line}}', line)}`,
              tag: tag
            });
          }

          if (tag.match(Msg.colspan.reg)) {
            errorsFounded++;

            msgArr.push({
              line: line,
              msg: `${Msg.colspan.message.replace('{{line}}', line)}`,
              tag: tag
            });
          }

          if (tag.match(Msg.div.reg)) {
            errorsFounded++;

            msgArr.push({
              line: line,
              msg: `${Msg.div.message.replace('{{line}}', line)}`,
              tag: tag
            });
          }

          if (tag.match(Msg.tagfont.reg)) {
            errorsFounded++;

            msgArr.push({
              line: line,
              msg: `${Msg.tagfont.message.replace('{{line}}', line)}`,
              tag: tag
            });
          }

          if (tagName == 'meta') {
            if (!tag.match(Msg.utf8.reg)) {
              errorsFounded++;

              msgArr.push({
                line: line,
                msg: `${Msg.utf8.message}`,
                tag: tag
              });
            }
          }

          if (tagName == 'html') {
            if (!str.match(Msg.doctype.reg)) {
              errorsFounded++;

              msgArr.push({
                line: '1',
                msg: `${Msg.doctype.message}`,
                tag: '<!DOCTYPE>'
              });
            }
          }

          if (tagName == 'td') {
            if (tag.match(Msg.legalTextTd.reg)) {
              let tagLegal = str.substring(node.startOffset, node.endTag.endOffset);
              // console.log(tagLegal);

              if (!tagLegal.match(Msg.legaltext.reg)) {
                errorsFounded++;

                msgArr.push({
                  line: result[i].line,
                  msg: `${Msg.legaltext.message.replace('{{line}}', line)}`,
                  tag: result[i + 1].tag
                });
              } else {

                if (tagLegal.match(Msg.hasLinkInLegalText.reg) !== null) {
                  errorsFounded++;

                  msgArr.push({
                    line: result[i].line,
                    msg: `${Msg.hasLinkInLegalText.message.replace('{{line}}', line)}`,
                    tag: line
                  });
                } else {
                  if (tagLegal.match(Msg.ilegalLink.reg) !== null) {
                    errorsFounded++;

                    msgArr.push({
                      line: result[i + 2].line,
                      msg: `${Msg.ilegalLink.message}`,
                      tag: result[i + 1].tag
                    });
                  }

                  if (tagLegal.match(Msg.hasTagSpan.reg) === null) {
                    if (tagLegal.match(Msg.link.reg) !== null) {
                      errorsFounded++;

                      msgArr.push({
                        line: result[i + 2].line,
                        msg: `${Msg.link.message}`,
                        tag: result[i + 1].tag
                      });
                    }
                  }
                }
              }
            }
          }
        }
      });

      // console.log(errorsFounded, Msg.hasLinkInLegalText.reg, Msg.ilegalLink.reg);
      console.log(errorsFounded);
    });

    callback(msgArr);
  }

  configureReadLineResult(str, callback) {
    const readline = ReadLine(str);
    const result = [];

    readline.forEach((node) => {
      if (node.nodeName === '#text') {
        let textNode = node.value.replace(/[-[\]{}()*+?.,\\^$|#\s]/gi, ' ').replace(/\s+/g, ' ');

        if (textNode !== ' ') {
          result.push({
            tagName: node.nodeName,
            text: textNode,
            line: node.line
          });
        }
      } else {
        if (node.tag) {
          result.push({
            tagName: node.nodeName,
            tag: node.tag,
            endTag: node.endTag,
            startOffset: node.startOffset,
            line: node.line,
          });
        }
      }
    });

    callback(result);
  }

  /**
   * Pega o html e retorna um objeto com uma lista de tags e os numeros das linhas
   *
   * @method readline
   *
   * @param {String} str HTML no formato string
   * @param {Function} callback Função para o retorno do resultado
   */
  /*readline(str, callback) {
    let newStr = str.replace(/((alt=")((.*(\r\n|(\r\n(\s+|)))+.*)|(.*\s.*))("))/g, function (m) {
      let reg = m.toString().replace(/(\r\n)+|\r+|\n+|\t+/i, ' ');
      reg = reg.replace(/\s\s+/g, ' ');

      return reg;
    });

    const myInterface = rl.createInterface({
      input: stringToStream(newStr)
    });

    let line = 0;
    let objTags = [];

    myInterface.on('line', (text) => {
      line++;
      // console.log('-> ' + text);
      objTags.push({
        tag: text,
        line: line
      });
    });

    myInterface.on('close', () => {
      if (callback != null) {
        callback(objTags);
      }
    });
  }*/

  without(array, what) {
    return array.filter((element) => element !== what);
  }
}

/*
{
  w3c: '',
  validation: [
    '',...
  ]
}

*/

export default (htmlStr) => {
  return new Validation(htmlStr);
};
